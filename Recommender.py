import TFIDFModelGenerator as tfidf
from Repositories import UserRepository as ur, RatingsRepository as rr, MovieRepository as mr
import math as m


def getUserInput():
    print("Introduce your user id:")
    user = input()
    return user


def loadTFIDFModel():
    TFIDFModel = tfidf.TFIDFModel()
    TFIDFModel.loadTFIDFDictionary()
    return TFIDFModel.TFIDFDictionary


def makeRecommendations(user):
    print("Generating your user profile...")
    filmsWatchedByUser = ratingsRepository.ratingsDict[user]
    totalRating = 0
    for rating in filmsWatchedByUser:
        totalRating += rating.rating

    avgRating = totalRating / len(filmsWatchedByUser)
    filmsAdjustedByUserRating = {}

    for rating in filmsWatchedByUser:
        filmsAdjustedByUserRating[int(rating.idItem)] = rating.rating - avgRating

    userProfileDictionary = {}
    for rating in filmsWatchedByUser:
        userProfileDictionary[rating.idItem] = {}
        listOfTags = TFIDFDictionary[rating.idItem]
        for tag in listOfTags:
            if tag not in listOfTags:
                userProfileDictionary[rating.idItem][tag] = 0

            userProfileDictionary[rating.idItem][tag] = TFIDFDictionary[rating.idItem][tag] * filmsAdjustedByUserRating[
                rating.idItem]

    userProfileFinal = {}
    for movie in userProfileDictionary:
        for tag in userProfileDictionary[movie]:
            if tag not in userProfileFinal:
                userProfileFinal[tag] = userProfileDictionary[movie][tag]
            else:
                valueToAdd = userProfileDictionary[movie][tag]
                userProfileFinal[tag] = userProfileFinal[tag] + valueToAdd

    movieRecommendation = {}
    print("Finding movies for you...")
    for movie in TFIDFDictionary:
        if movie not in userProfileDictionary:
            cocienteSumatoria = 0
            sumatoriaTagsUsuario = 0
            sumatoriaTagsProducto = 0
            for tag in TFIDFDictionary[movie]:
                if tag in userProfileFinal:
                    cocienteSumatoria += TFIDFDictionary[movie][tag] * userProfileFinal[tag]
                sumatoriaTagsProducto += m.pow(TFIDFDictionary[movie][tag], 2)

            for tag in userProfileFinal:
                sumatoriaTagsUsuario += m.pow(userProfileFinal[tag], 2)

            sqrtSumatoriaTagsUsuario = m.sqrt(sumatoriaTagsUsuario)
            sqrtSumatoriaTagsProducto = m.sqrt(sumatoriaTagsProducto)
            dividendo = sqrtSumatoriaTagsUsuario * sqrtSumatoriaTagsProducto

            coseno = cocienteSumatoria / dividendo

            movieRecommendation[movie] = coseno

    final = sorted(movieRecommendation.items(), key=lambda x: x[1], reverse=True)
    return final[:10]


print("Generating TFIDF Model")
TFIDFDictionary = loadTFIDFModel()

print("TFIDF Model generated")
print("Loading additional information from .csv files")
excelRatings = "csvFiles/ratings.csv"
ratingsRepository = rr.RatingsRepository()
ratingsRepository.loadRatings(excelRatings)

excelMoviesName = "csvFiles/movie-titles.csv"
moviesRepository = mr.MovieRepository()
moviesRepository.loadMovies(excelMoviesName)
movies = moviesRepository.moviesDict

excelUsers = "csvFiles/users.csv"
userRepository = ur.UserRepository()
userRepository.loadUsers(excelUsers)
usersRegistered = userRepository.usersIDs

recommendationsDictionary = {}
print("Loading finished")
print("Introduce 0 to quit the program")
user = -1
run = True
while run:
    user = getUserInput()
    user = int(user)
    if user == 0:
        run = False

    if user not in usersRegistered:
        print("No user with this ID found")
    else:
        if user not in recommendationsDictionary:
            recomendaciones = makeRecommendations(user)
        else:
            recomendaciones = recommendationsDictionary[user]

        print("Top 10 recommendations for user", user)
        for idx, value in enumerate(recomendaciones):
            print(idx + 1, "-", movies[value[0]], "->", value[1])

        recommendationsDictionary[user] = recomendaciones

print("Good bye...")
