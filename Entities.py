class Movie:
    def __init__(self, idMovie, title):
        self.idMovie = idMovie
        self.title = title


class Rating:
    def __init__(self, idUser, idItem, rating):
        self.idUser = idUser
        self.idItem = idItem
        self.rating = rating


class Tag:
    def __init__(self, idItem, tagContent):
        self.idItem = idItem
        self.tagContent = tagContent


class User:
    def __init__(self, idUser, desc):
        self.idUser = idUser
        self.desc = desc
