import pandas as pd
import Entities as e

class MovieRepository:

    def __init__(self):
        self.movies = []
        self.moviesDict = {}

    def loadMovies(self, excel_file):
        moviesData = pd.read_csv(excel_file, header = None)
        columns = moviesData.columns

        for index, row in moviesData.iterrows():
            newMovie = e.Movie(row[columns[0]], row[columns[1]])
            self.movies.append(newMovie)
            self.moviesDict[newMovie.idMovie] = newMovie.title

    def printMovies(self):
        for movie in self.movies:
            print("Movie with id: ", movie.idMovie, "and name: ", movie.title)

    def countMoviesInRepository(self):
        return len(self.movies)

