import pandas as pd
import Entities as e


class UserRepository:

    def __init__(self):
        self.users = []
        self.usersIDs = []

    def loadUsers(self, excel_file):
        usersData = pd.read_csv(excel_file, header = None)
        columns = usersData.columns
        test = 0
        for index, row in usersData.iterrows():
            newUser = e.User(row[columns[0]], row[columns[1]])
            self.users.append(newUser)
            self.usersIDs.append(newUser.idUser)

    def printUsers(self):
        for user in self.users:
            user.toString()

    def countUsersInRepository(self):
        return len(self.users)

