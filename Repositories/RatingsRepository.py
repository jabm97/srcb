import pandas as pd
import Entities as e


class RatingsRepository:

    def __init__(self):
        self.ratings = []
        self.ratingsDict = {}

    def loadRatings(self, excel_file):
        data = pd.read_csv(excel_file, header=None)
        columns = data.columns

        for index, row in data.iterrows():
            elem = e.Rating(row[columns[0]], row[columns[1]], row[columns[2]])
            self.ratings.append(elem)
            if elem.idUser not in self.ratingsDict:
                self.ratingsDict[elem.idUser] = []

            self.ratingsDict[elem.idUser].append(elem)

    def print(self):
        for elem in self.ratings:
            elem.toString()

    def countRatingsInRepository(self):
        return len(self.ratings)


