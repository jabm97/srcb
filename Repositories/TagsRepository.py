import pandas as pd
import Entities as e

class TagsRepository:

    def __init__(self):
        self.tags = []

    def loadTags(self, excel_file):
        data = pd.read_csv(excel_file, header = None, encoding='latin-1')
        columns = data.columns

        for index, row in data.iterrows():
            elem = e.Tag(row[columns[0]], row[columns[1]])
            self.tags.append(elem)

    def print(self):
        for elem in self.tags:
            elem.toString()

    def countTagsInRepository(self):
        return len(self.tags)
