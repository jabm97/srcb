import math as m
from Repositories import MovieRepository as mr, TagsRepository as tr


class TFIDFModel():

    def __init__(self):
        self.TFIDFDictionary = {}

    def loadTFIDFDictionary(self):
        excelTagsName = "csvFiles/movie-tags.csv"
        excelMoviesName = "csvFiles/movie-titles.csv"

        existingTags = []
        tagsRepository = tr.TagsRepository()
        tagsRepository.loadTags(excelTagsName)
        etiquetasCSV = tagsRepository.tags

        for etiqueta in etiquetasCSV:
            if etiqueta.tagContent not in existingTags:
                existingTags.append(etiqueta.tagContent)

        existingMovies = []
        moviesRepository = mr.MovieRepository()
        moviesRepository.loadMovies(excelMoviesName)
        moviesCSV = moviesRepository.movies

        TFDictionary = {}
        IDFDictionary = {}

        for movie in moviesCSV:
            TFDictionary[movie.idMovie] = {}
            for tag in etiquetasCSV:
                if tag.idItem == movie.idMovie:
                    #Fill TFDictionary
                    if tag.tagContent not in TFDictionary[movie.idMovie]:
                        TFDictionary[movie.idMovie][tag.tagContent] = 1
                    else:
                        TFDictionary[movie.idMovie][tag.tagContent] += 1

                    # FIll IDFDictionary
                    if tag.tagContent not in IDFDictionary:
                        IDFDictionary[tag.tagContent] = []
                    if movie.idMovie not in IDFDictionary[tag.tagContent]:
                        IDFDictionary[tag.tagContent].append(movie.idMovie)

        totalProducts = len(moviesCSV)
        IDFDictionaryUpdated = {}
        #Use previous values calculated to obtain the actual IDF value
        for tag in IDFDictionary:
            IDFDictionaryUpdated[tag] = 0
            numberOfProductsWithTag = len(IDFDictionary[tag])
            divisionTotalByProductsWithTag = totalProducts / numberOfProductsWithTag
            IDFDictionaryUpdated[tag] = m.log(divisionTotalByProductsWithTag, 10)

        TFIDFDictionary = TFDictionary
        #With the TF and IDF calculated previously, obtain the TFIDF for each tag, for each movie.
        for movie in TFIDFDictionary:
            sumOfTFIDFValues = 0

            for tag in TFIDFDictionary[movie]:
                TFvalue = TFIDFDictionary[movie][tag]
                IDFvalue = IDFDictionaryUpdated[tag]
                TFIDFDictionary[movie][tag] = TFvalue * IDFvalue
                sumOfTFIDFValues += m.pow(TFIDFDictionary[movie][tag], 2)

            dividendoNormalizacion = m.sqrt(sumOfTFIDFValues)

            for tag in TFIDFDictionary[movie]:
                TFIDFDictionary[movie][tag] = TFIDFDictionary[movie][tag] / dividendoNormalizacion

        self.TFIDFDictionary = TFIDFDictionary